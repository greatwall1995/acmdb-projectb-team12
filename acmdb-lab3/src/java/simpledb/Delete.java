package simpledb;

import java.io.IOException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     * 
     * @param t
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
	TransactionId tid;
	DbIterator child;
	TupleDesc desc;
	boolean vis;
    
	public Delete(TransactionId tid, DbIterator child) {
        // some code goes here
		this.tid = tid;
		this.child = child;
		desc = new TupleDesc(new Type[]{Type.INT_TYPE});
		vis = false;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        return desc;
    }

    public void open() throws DbException, TransactionAbortedException {
        // some code goes here
        child.open();
        super.open();
		vis = false;
    }

    public void close() {
        // some code goes here
		super.close();
        child.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        // some code goes here
		close();
        open();
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     * 
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        // some code goes here
		if (!vis) {
			int tot = 0;
			vis = true;
			if (child == null) return null;
			while (child.hasNext()) {
				++tot;
				try {
					Database.getBufferPool().deleteTuple(tid, child.next());
				} catch (IOException e) {
					return null;
				}
			}
			Tuple ans = new Tuple(desc);
			ans.setField(0, new IntField(tot));
			return ans;
		} else {
			return null;
		}
    }

    @Override
    public DbIterator[] getChildren() {
        // some code goes here
        return new DbIterator[]{child};
    }

    @Override
    public void setChildren(DbIterator[] children) {
        // some code goes here
		child = children[0];
    }

}

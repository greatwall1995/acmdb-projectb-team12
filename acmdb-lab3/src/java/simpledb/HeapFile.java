package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {
	
	TupleDesc td;
	final int tableId;
	File file;
	int pgSize;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
		// some code goes here
		this.td = td;
		this.tableId = f.getAbsoluteFile().hashCode();
		this.file = f;
		this.pgSize = BufferPool.getPageSize();
		try {
			Database.getCatalog().addTable(this);
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return tableId;
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
		return td;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        if (pid.getTableId() != tableId) return null;
		try {
            if (pid.pageNumber() == numPages()) {
                Page page = new HeapPage((HeapPageId) pid, HeapPage.createEmptyPageData());
                writePage(page);
                return page;
            } else {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
                randomAccessFile.seek(BufferPool.getPageSize() * pid.pageNumber());
                byte[] data = new byte[BufferPool.getPageSize()];
                randomAccessFile.read(data);
                randomAccessFile.close();
                return new HeapPage((HeapPageId) pid, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoSuchElementException();
        }
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
        RandomAccessFile randomAccessFile;
        randomAccessFile = new RandomAccessFile(file, "rw");
        randomAccessFile.seek(BufferPool.getPageSize() * page.getId().pageNumber());
        randomAccessFile.write(page.getPageData());
        randomAccessFile.close();
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        return (int) file.length() / BufferPool.getPageSize();
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
		try {
			ArrayList<Page> res = new ArrayList<Page>();
			for (int i = 0; i < numPages(); ++i) {
				PageId pid = new HeapPageId(getId(), i);
				//System.out.printf("%d %d\n", i, numPages());
				HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
				if (page.getNumEmptySlots() > 0) {
					page.insertTuple(t);
					res.add(page);
					return res;
				}
			}
			HeapPageId pid = new HeapPageId(getId(), numPages());
			HeapPage page = new HeapPage(pid, new byte[pgSize]);
			page = ((HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE));
			page.insertTuple(t);
			res.add(page);
			return res;
		} catch (Exception e) {
			throw e;
		}
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
        try {
			PageId pageId = t.getRecordId().getPageId();
			HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pageId, Permissions.READ_WRITE);
			//System.out.printf("%d %d\n", pageId.pageNumber(), numPages());
			page.deleteTuple(t);
			page.markDirty(true, tid);
			return null;
		} catch (Exception e) {
			throw e;
		}
    }

    // see DbFile.java for javadocs
	private class HeapFileIterator implements DbFileIterator {
		
		int size, i, tableId;
		HeapPage page;
		TransactionId tid;
		Iterator<Tuple> it;
		public HeapFileIterator(TransactionId tid, int tableId) {
			this.tid = tid;
			this.tableId = tableId;
			i = -1;
		}
		
		/**
		* Opens the iterator
		* @throws DbException when there are problems opening/accessing the database.
		*/
		public void open() throws DbException, TransactionAbortedException {
			i = 0;
			page = (HeapPage)Database.getBufferPool().getPage(tid, new HeapPageId(tableId, 0), Permissions.READ_WRITE);
			it = page.iterator();
		}
	
		/** @return true if there are more tuples available, false if no more tuples or iterator isn't open. */
		public boolean hasNext() throws DbException, TransactionAbortedException {
			if (i < 0 || it == null) return false;
			int ti = i;
			Iterator<Tuple> tit = it;
			while (!tit.hasNext()) {
				if (ti == numPages() - 1) return false;
				try {
					HeapPage tpage = (HeapPage)Database.getBufferPool().getPage(tid, new HeapPageId(tableId, ++ti), Permissions.READ_WRITE);
					tit = tpage.iterator();
				} catch (Exception e) {
					throw e;
				}
			}
			return true;
		}
	
		/**
		* Gets the next tuple from the operator (typically implementing by reading
		* from a child operator or an access method).
		*
		* @return The next tuple in the iterator.
		* @throws NoSuchElementException if there are no more tuples
		*/
		public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
			try {
				if (!hasNext()) throw new NoSuchElementException();
				if (it.hasNext()) {
					return it.next();
				} else {
					try {
						page = (HeapPage)Database.getBufferPool().getPage(tid, new HeapPageId(tableId, ++i), Permissions.READ_WRITE);
					} catch (TransactionAbortedException e) {
						throw e;
					} catch (DbException e) {
						throw e;
					}
					it = page.iterator();
					return it.next();
				}
			} catch (TransactionAbortedException e) {
				throw e;
			}
		}
	
		/**
		* Resets the iterator to the start.
		* @throws DbException When rewind is unsupported.
		*/
		public void rewind() throws DbException, TransactionAbortedException {
			open();
		}
	
		/**
		* Closes the iterator.
		*/
		public void close() {
			i = -1;
		}
	}
	
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
		HeapFileIterator res = new HeapFileIterator(tid, tableId);
        return res;
    }

}

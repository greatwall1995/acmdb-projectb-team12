package simpledb;
import java.util.*;
/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */
	int gbfield;
	Type gbfieldtype;
	int afield;
	Op what;
	
	Map<Field, Integer> ans = null;

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
		this.gbfield = gbfield;
        this.gbfieldtype = gbfieldtype;
		this.afield = afield;
        this.what = what;
    	ans = new HashMap<Field,Integer>();
		if (what != Op.COUNT) throw new IllegalArgumentException("NOT COUNT!");
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
		Field gb_val = null;
    	if (gbfield != NO_GROUPING) gb_val = tup.getField(gbfield);
    	
    	if (!ans.containsKey(gb_val)) { 
			ans.put(gb_val, 1);
    		
    	}
		else {
			//Integer curValue = ((IntField)tup.getField(afield)).getValue();
			if (what == Op.COUNT) ans.put(gb_val, (ans.get(gb_val) + 1));
			//if (what == Op.SUM) ans.put(gb_val, (ans.get(gb_val) + curValue));
			//if (what == Op.AVG) ans.put(gb_val, avg_count.get(gb_val).get(0) / avg_count.get(gb_val).get(1));;
			//if (what == Op.MIN) ans.put(gb_val, Math.min(ans.get(gb_val), curValue));
			//if (what == Op.MAX) ans.put(gb_val, Math.max(ans.get(gb_val), curValue));
    	}
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        List<Tuple> tuples = new ArrayList<Tuple>();
    	TupleDesc td = null;
    	if (gbfield != NO_GROUPING) {
	    	td = new TupleDesc(new Type[] {gbfieldtype, Type.INT_TYPE});
	    	for (Field key : ans.keySet()) {
	    		Tuple nTup = new Tuple(td);
	    		nTup.setField(0, key);
	    		nTup.setField(1, new IntField(ans.get(key)));
	    		tuples.add(nTup);
	    	}
    	}
		else {
	    	td = new TupleDesc(new Type[]{Type.INT_TYPE});
	    	for (Field key : ans.keySet()) {
	    		Tuple nTup = new Tuple(td);
	    		nTup.setField(0, new IntField(ans.get(key)));
	    		tuples.add(nTup);
	    	}
    	}
    	return new TupleIterator(td,tuples);
    }

}

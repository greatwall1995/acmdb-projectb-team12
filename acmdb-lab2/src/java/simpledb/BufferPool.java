package simpledb;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 * 
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    /** Bytes per page, including header. */
    private static final int PAGE_SIZE = 4096;

    private static int pageSize = PAGE_SIZE;
    
    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;
	List<Page> cache;
	List<PageId> pid_list;
	List<Permissions> perm_list;
	List<TransactionId> tid_list;
	Map<PageId, Integer> idx;
	int pos, nPages;
	
    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        // some code goes here
		nPages = numPages;
		cache = new ArrayList<Page>();
		perm_list = new ArrayList<Permissions>();
		tid_list = new ArrayList<TransactionId>();
		pid_list = new ArrayList<PageId>();
		for (int i = 0; i < numPages; ++i) {
			cache.add(null);
			perm_list.add(null);
			tid_list.add(null);
			pid_list.add(null);
		}
		idx = new HashMap<PageId, Integer>();
		pos = 0;
    }
    
    public static int getPageSize() {
      return pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void setPageSize(int pageSize) {
    	BufferPool.pageSize = pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void resetPageSize() {
    	BufferPool.pageSize = PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException {
		if (idx.get(pid) == null) {
			try {
				if (cache.get(pos) != null) {
					evictPage(pos);
				}
				Page page = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
				cache.set(pos, page);
				perm_list.set(pos, perm);
				tid_list.set(pos, tid);
				pid_list.set(pos, pid);
				idx.put(pid, pos++);
				pos %= nPages;
				return page;
			} catch (Exception e) {
				throw e;
			}
		} else {
			return cache.get(idx.get(pid));
		}
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public  void releasePage(TransactionId tid, PageId pid) {
        // some code goes here
        // not necessary for lab1|lab2
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId p) {
        // some code goes here
        // not necessary for lab1|lab2
        return false;
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
    }

    /**
     * Add a tuple to the specified table on behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to and any other 
     * pages that are updated (Lock acquisition is not needed for lab2). 
     * May block if the lock(s) cannot be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
		try {
			Database.getCatalog().getDatabaseFile(tableId).insertTuple(tid, t);
		} catch (Exception e) {
			throw e;
		}
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from and any
     * other pages that are updated. May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
		try {
			Database.getCatalog().getDatabaseFile(t.getRecordId().getPageId().getTableId()).deleteTuple(tid, t);
		} catch (Exception e) {
			throw e;
		}
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        // some code goes here
        // not necessary for lab1
		for (int i = 0; i < nPages; ++i) {
			if (cache.get(i) != null) {
				try {
					flushPage(cache.get(i));
				} catch (IOException e) {
					throw e;
				}
			}
		}
    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
        
        Also used by B+ tree files to ensure that deleted pages
        are removed from the cache so they can be reused safely
    */
    public synchronized void discardPage(PageId pid) {
        // some code goes here
        // not necessary for lab1
		if (idx.get(pid) == null) return;
		int pos = idx.get(pid);
		cache.set(pos, null);
		perm_list.set(pos, null);
		tid_list.set(pos, null);
		pid_list.set(pos, null);
		idx.remove(pid);
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized  void flushPage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
		try {
			PageId pid = page.getId();
			page.markDirty(false, tid_list.get(idx.get(pid)));
			page.setBeforeImage();
			Database.getCatalog().getDatabaseFile(pid.getTableId()).writePage(page);
		} catch (IOException e) {
			throw e;
		}
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized  void flushPages(TransactionId tid) throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized  void evictPage(int pos) throws DbException {
        // some code goes here
        // not necessary for lab1
		try {
			Page page = cache.get(pos);
			if (page.isDirty() != null) {
				flushPage(page);
			}
			cache.set(pos, null);
			perm_list.set(pos, null);
			tid_list.set(pos, null);
			pid_list.set(pos, null);
			idx.remove(page.getId());
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}

//evictPage()
//ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
//ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) 